//////////////////////////////////////////////////////////////////////////////////////////////////
// Microservice router requirements
//////////////////////////////////////////////////////////////////////////////////////////////////
import { Router,  Request, Response } from 'express';
import { FacadeMicroservice } from './facade';
var passport = require('../config/passport.js');

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * microservice router controller
 */
export const controller  = {
	/**greetings
	 *
	 * test method from microservice
	 */
	greetings : (req: Request, res: Response) => {
		FacadeMicroservice.greet(req.body)
		.then((data) => {
			res.status(200).json(data)
		})
		.catch((err) => {
			res.status(500).send(err)
		})
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * microservice router
 */
export const microservice: Router = Router()
microservice.post('/greeting', controller.greetings)
microservice.get('/auth/google', passport.authenticate("google", { scope: ["profile", "email"] }))
microservice.get('/auth/google/callback', passport.authenticate("google", { failureRedirect: "http://localhost:8080/", session: false }),
	function(req, res) {
		if(req.user.email.split('@')[1] == 'simplusinnovation.com') {
			var token = req.user.token;
			res.redirect(`http://localhost:8080/#/home?token=${token}&username=${req.user.name}&&valid=true`);
		} else {
			var token = req.user.token;
			res.redirect(`http://localhost:8080/#/home?&valid=false`);
		}
});

export default microservice