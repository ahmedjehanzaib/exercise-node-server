var passport = require("passport");
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
passport.serializeUser(function(user, done) {
 done(null, user);
});
passport.deserializeUser(function(user, done) {
 done(null, user);
});
passport.use(
 new GoogleStrategy(
  {
   clientID: "247763698397-pgboc29nplrasnlvrrus3pqhtlgsfsm9.apps.googleusercontent.com",
   clientSecret: "SfaWFe94ylHXSqHc5zuHdslA",
   callbackURL: "http://localhost:3002/api/v1/microservice/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, done) {
   var userData = {
    email: profile.emails[0].value,
    name: profile.displayName,
    token: accessToken
   };
   done(null, userData);
  }
 )
);

module.exports = passport;